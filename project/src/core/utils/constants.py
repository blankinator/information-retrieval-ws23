import os

# logging specific constants
LOGGING_LEVEL = os.getenv('LOGGING_LEVEL', 'INFO')
LOGGING_FORMAT = os.getenv('LOGGING_FORMAT', '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
LOGGING_DATE_FORMAT = os.getenv('LOGGING_DATE_FORMAT', '%Y-%m-%d %H:%M:%S')

# api specific constants

# main api specific constants
MAIN_API_PORT = int(os.getenv('MAIN_API_PORT', 8000))

# storage api specific constants
STORAGE_API_PORT = int(os.getenv('STORAGE_API_PORT', 8001))
STORAGE_API_DATABASE_HOST = os.getenv('STORAGE_API_DATABASE_HOST', 'localhost')
STORAGE_API_DATABASE_PORT = os.getenv('STORAGE_API_DATABASE_PORT', '27017')
STORAGE_API_DATABASE_NAME = os.getenv('STORAGE_API_DATABASE_NAME', 'information_retrieval')

INVERTED_INDEX_COLLECTION_NAME = os.getenv('INVERTED_INDEX_COLLECTION_NAME', 'inverted_index')
DOCUMENT_CORPUS_COLLECTION_NAME = os.getenv('DOCUMENT_CORPUS_COLLECTION_NAME', 'document_corpus')
ANNOTATIONS_COLLECTION_NAME = os.getenv('ANNOTATIONS_COLLECTION_NAME', 'annotations')
