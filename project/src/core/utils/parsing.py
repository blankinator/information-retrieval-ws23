import os

import pandas as pd


def create_run_file(results: pd.DataFrame | list[dict],
                    run_name: str,
                    output_path: str,
                    overwrite: bool = False) -> None:
    """
    Creates a run file from the results
    :param results: list or dataframe of results
    :param run_name: name of the run
    :param output_path: path to save the run file to
    :param overwrite: whether to overwrite the file if it already exists
    :return: None if successful
    """

    if os.path.isfile(output_path) and not overwrite:
        raise FileExistsError("run file already exists")

    if isinstance(results, pd.DataFrame):
        results = results.to_dict("records")

    with open(output_path, "w") as f:
        for result in results:
            f.write(
                f"{result['qid']} Q0 {result['docno']} {result['rank']} {result['score']} {run_name}\n"
            )
