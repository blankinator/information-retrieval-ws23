import sys
import logging

from core.utils.constants import LOGGING_FORMAT, LOGGING_DATE_FORMAT


def get_logger(name: str) -> logging.Logger:
    """
    Get logger with name.
    :param name: name of the logger to create
    :return: logger object with name
    """

    # initialize logger
    logger = logging.getLogger(name)
    logger.setLevel(logging.INFO)
    formatter = logging.Formatter(
        LOGGING_FORMAT,
        LOGGING_DATE_FORMAT
    )
    # add file handler
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    # add sys output handler
    sh = logging.StreamHandler(sys.stdout)
    sh.setLevel(logging.INFO)
    sh.setFormatter(formatter)
    logger.addHandler(sh)

    return logger
