from typing import Iterable


def prepare_queries(queries: Iterable) -> list[dict]:
    """
    Prepares the queries for retrieval
    :param queries: list of queries, query_id will be translated to qid
    :return: list of queries
    """

    output_queries = [
        {
            "qid": query.query_id if hasattr(query, "query_id") else query.qid,
            "query": query.title,
            "description": query.description,
            "narrative": query.narrative,
        }
        for query in queries
    ]

    return output_queries
