import pyterrier as pt
from jnius.jnius import JavaException
import pandas as pd


def retrieve_for_query(query, retrieval_model=pt.batchretrieve.BatchRetrieveBase):
    """
    Runs the retrieval model for a single given query
    :param query: query to run the retrieval model for, must contain a query attribute
    :param retrieval_model: retrieval model to use, must contain a search method
    :return: result of the retrieval model
    """
    result = retrieval_model.search(query["query"])
    return result


def retrieve_for_queries(queries: list[dict],
                         retrieval_model=pt.batchretrieve.BatchRetrieveBase) -> pd.DataFrame:
    """
    Runs the retrieval model for the given queries
    :param queries: list of queries, must contain a qid and title attribute
    :param retrieval_model: model to use for retrieval, must contain transform method
    :return: query results as dataframe
    """

    if not isinstance(queries, pd.DataFrame):
        try:
            queries = pd.DataFrame(queries)
        except:
            raise ValueError("queries must be convertible to a dataframe")

    try:
        results = retrieval_model.transform(queries)
    except JavaException as e:
        if "QueryParserException" in e.args[0]:
            raise ValueError("Invalid query text found")
        else:
            raise e
    return results
