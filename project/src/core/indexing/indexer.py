from typing import Iterable

import pyterrier as pt


def create_sparse_indexer(index_path: str,
                          pre_pipeline: Iterable[pt.transformer.TransformerBase] = None,
                          stemmer: pt.transformer.TransformerBase = pt.index.TerrierStemmer.porter,
                          stopwords: pt.transformer.TransformerBase = pt.index.TerrierStopwords,
                          tokeniser: pt.transformer.TransformerBase = pt.index.TerrierTokeniser.english,
                          **kwargs):
    """
    Creates a sparse indexer pipeline with the given parameters.
    :param index_path: path to save the index to
    :param pre_pipeline: TransformerBase pipeline to apply before indexing
    :param stemmer: stemmer to use
    :param stopwords: stopwords to use
    :param tokeniser: tokeniser to use
    :param kwargs: additional arguments to pass to the indexer
    :return:
    """

    if pre_pipeline is None:
        pre_pipeline = list()

    if len(pre_pipeline) > 0:
        for i in range(len(pre_pipeline)):
            if i == 0:
                indexer = pre_pipeline[i]
            elif i == len(pre_pipeline):
                indexer = indexer >> pt.IterDictIndexer(index_path,
                                                        stemmer=stemmer,
                                                        stopwords=stopwords,
                                                        tokeniser=tokeniser,
                                                        **kwargs)
            else:
                indexer = indexer >> pre_pipeline[i]

    else:
        indexer = pt.IterDictIndexer(index_path,
                                     stemmer=stemmer,
                                     stopwords=stopwords,
                                     tokeniser=tokeniser,
                                     **kwargs)

    return indexer
