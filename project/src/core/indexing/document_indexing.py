from typing import Iterable

import pyterrier as pt


def index_documents(documents: Iterable[dict], indexer: pt.index.TerrierIndexer) -> pt.index.IndexRef:
    """
    Indexes the given documents and returns the index.
    :param documents: iterable of documents to index, docs must have a docno and text field
    :param indexer: indexer to use
    :return: index with newly indexed documents
    """
    index_ref = indexer.index(documents)
    return index_ref
