# Storage API

This is the Storage API for the Information Retrieval project.

## Prerequisites

See [Prerequisites](../../../deployment/storage_api/README.md#prerequisites) for docker deployment.

- [Python 3.10](https://www.python.org/downloads/)
- Install dependencies: `pip install -r requirements.txt`
- Set environment variables:
    - `STORAGE_API_PORT`: Port to run the Storage API on
    - `STORAGE_API_DB_HOST`: Hostname of the database
    - `STORAGE_API_DB_PORT`: Port of the database
    - `STORAGE_API_DB_NAME`: Name of the database (defaults to `information retrieval`)
- Run `python main.py`
- Documentation is available at `http://{HOST}:{PORT}/docs`

## Environment Variables

| Variable              | Description                                                |
|-----------------------|------------------------------------------------------------|
| `STORAGE_API_PORT`    | Port to run the Storage API on                             |
| `STORAGE_API_DB_HOST` | Hostname of the database                                   |
| `STORAGE_API_DB_PORT` | Port of the database                                       |
| `STORAGE_API_DB_NAME` | Name of the database (defaults to `information retrieval`) |

## API

### `GET /documents`
Returns a list of all documents in the database.


### `GET /documents/{id}`
Returns the document with the given id.

### `POST /documents`
Adds a document to the database.
Fields are specified in body.

### `PUT /documents/{id}`
Updates the document with the given id.
Fields are specified in body.

### `DELETE /documents/{id}`
Deletes the document with the given id.

### `GET /inverted_index`
Returns the inverted index.

### `GET /inverted_index/{term}`
Returns the inverted index entry for the given term.

### `POST /inverted_index/{term}`
Adds a term to the inverted index.
Fields are specified in body.

### `PUT /inverted_index/{term}`
Updates the inverted index entry for the given term.
Fields are specified in body.

### `DELETE /inverted_index/{term}`
Deletes the inverted index entry for the given term.

### `GET /annotations`
Returns a list of all annotations in the database.

### `GET /annotations/{id}`
Returns the annotation with the given id.

### `POST /annotations`
Adds an annotation to the database.
Fields are specified in body.

### `PUT /annotations/{id}`
Updates the annotation with the given id.
Fields are specified in body.

### `DELETE /annotations/{id}`
Deletes the annotation with the given id.
