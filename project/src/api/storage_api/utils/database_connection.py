import sys
import pymongo

from core.utils.constants import STORAGE_API_DATABASE_NAME, STORAGE_API_DATABASE_HOST, STORAGE_API_DATABASE_PORT
from core.utils.logging import get_logger

logger = get_logger(__name__)


def init_db():
    """
    Initialize database connection.
    :return: pymongo database connection
    """
    logger.info(
        f"Connecting to database {STORAGE_API_DATABASE_NAME} at {STORAGE_API_DATABASE_HOST}:{STORAGE_API_DATABASE_PORT}")
    try:
        client = pymongo.MongoClient(STORAGE_API_DATABASE_HOST, int(STORAGE_API_DATABASE_PORT))
        db = client[STORAGE_API_DATABASE_NAME]
    except Exception as e:
        logger.error(
            f"Error connecting to database {STORAGE_API_DATABASE_NAME} at {STORAGE_API_DATABASE_HOST}:{STORAGE_API_DATABASE_PORT}")
        logger.error(e)
        sys.exit(1)

    return db


db = init_db()


def get_collection(collection_name: str):
    """
    Get collection from database.
    :param collection_name: name of the collection to get
    :return: pymongo collection object
    """
    return db[collection_name]
