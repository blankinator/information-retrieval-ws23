from typing import Any

from datetime import datetime
from bson import ObjectId


def parse(value: Any) -> Any:
    """
    Parses any type to be json serializable.
    :param value: value to be parsed
    :return: value parsed to be json serializable
    """

    def _parse(value: Any) -> Any:

        if isinstance(value, dict):
            return parse(value)
        elif isinstance(value, list):
            return [parse(item) for item in value]
        elif isinstance(value, datetime):
            return str(value)
        elif isinstance(value, ObjectId):
            return str(value)
        else:
            return value

    if isinstance(value, list):
        return [_parse(item) for item in value]
    elif isinstance(value, dict):
        return {key: _parse(value) for key, value in value.items()}
    else:
        return _parse(value)
