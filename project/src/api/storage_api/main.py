from fastapi import FastAPI
import uvicorn

from core.utils import constants
from core.utils.logging import get_logger
from api.storage_api.routes import terms, documents, annotations

logger = get_logger(__name__)

app = FastAPI()

# add routers to app
app.include_router(terms.router)
app.include_router(documents.router)
app.include_router(annotations.router)

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=constants.STORAGE_API_PORT)
