from fastapi import APIRouter, HTTPException, Body

from core.utils.constants import INVERTED_INDEX_COLLECTION_NAME
from core.utils.logging import get_logger
from api.storage_api.utils.database_connection import get_collection
from api.storage_api.utils.parsing import parse

logger = get_logger(__name__)
router = APIRouter()


@router.get("/inverted_index", tags=["inverted_index"], response_model=list[dict], status_code=200)
async def get_inverted_index():
    """
    Get all terms from inverted index
    """
    logger.info(f"Getting all terms from inverted index")

    inverted_index_collection = get_collection(INVERTED_INDEX_COLLECTION_NAME)
    terms_data = list(inverted_index_collection.find())

    if terms_data is None:
        logger.error(f"No terms found in inverted index")
        raise HTTPException(status_code=404, detail=f"No terms found in inverted index")

    return parse(terms_data)


@router.get("/inverted_index/{term}", tags=["inverted_index"], response_model=dict, status_code=200)
async def get_inverted_index_term(term: str):
    """
    Get term from inverted index
    """
    logger.info(f"Getting inverted index data for term: {term}")

    inverted_index_collection = get_collection(INVERTED_INDEX_COLLECTION_NAME)
    term_data = inverted_index_collection.find_one({"term": term})

    if term_data is None:
        logger.error(f"Term {term} not found in inverted index")
        raise HTTPException(status_code=404, detail=f"Term {term} not found in inverted index")

    return parse(term_data)


@router.post("/inverted_index/{term}", tags=["inverted_index"], response_model=dict, status_code=201)
async def post_inverted_index_term(term: str, fields=Body(...)):
    """
    Post term data to inverted index.

    Body should contain the data fields to be posted to the inverted index.
    """
    logger.info(f"Posting inverted index data for term: {term}")

    if fields is None:
        logger.error(f"Fields not specified")
        raise HTTPException(status_code=422, detail=f"Fields not specified")

    if len(fields) == 0:
        logger.error(f"Fields cannot be empty")
        raise HTTPException(status_code=422, detail=f"Fields cannot be empty")

    inverted_index_collection = get_collection(INVERTED_INDEX_COLLECTION_NAME)
    term_data = inverted_index_collection.find_one({"term": term})

    if term_data is not None:
        logger.error(f"Term {term} already exists in inverted index")
        raise HTTPException(status_code=409,
                            detail=f"Term {term} already exists in inverted index, use PUT instead to update it.")

    term_data = {
        "term": term,
        **fields
    }

    inverted_index_collection.insert_one(term_data)

    # fetch data again to get the _id field
    new_term_data = inverted_index_collection.find_one({"term": term})

    return parse(new_term_data)


@router.put("/inverted_index/{term}", tags=["inverted_index"], status_code=200)
async def put_inverted_index_term(term: str, fields=Body(...)):
    """
    Update term data in inverted index.

    Body should contain the data fields to be put to the inverted index.
    """
    logger.info(f"Updating inverted index data for term: {term}")

    if fields is None:
        logger.error(f"Fields not specified")
        raise HTTPException(status_code=422, detail=f"Fields not specified")

    if len(fields) == 0:
        logger.error(f"Fields cannot be empty")
        raise HTTPException(status_code=422, detail=f"Fields cannot be empty")

    inverted_index_collection = get_collection(INVERTED_INDEX_COLLECTION_NAME)
    term_data = inverted_index_collection.find_one({"term": term})

    if term_data is None:
        logger.error(f"Term {term} does not exist in inverted index, creating it...")
        return await post_inverted_index_term(term, fields)

    new_term_data = term_data | fields

    inverted_index_collection.update_one({"term": term}, {"$set": new_term_data})

    return parse(new_term_data)


@router.delete("/inverted_index/{term}", tags=["inverted_index"], status_code=204)
async def delete_inverted_index_term(term: str):
    """
    Delete term data from inverted index.
    """
    logger.info(f"Deleting inverted index data for term: {term}")

    inverted_index_collection = get_collection(INVERTED_INDEX_COLLECTION_NAME)
    term_data = inverted_index_collection.find_one({"term": term})

    if term_data is None:
        logger.error(f"Term {term} does not exist in inverted index")
        raise HTTPException(status_code=404, detail=f"Term {term} does not exist in inverted index")

    inverted_index_collection.delete_one({"term": term})

    return {}
