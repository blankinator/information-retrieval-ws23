from fastapi import APIRouter, HTTPException, Body

from core.utils.constants import ANNOTATIONS_COLLECTION_NAME
from core.utils.logging import get_logger
from api.storage_api.utils.database_connection import get_collection

logger = get_logger(__name__)

router = APIRouter()
COLLECTION_NAME = "annotations"


@router.get("/annotations", tags=["annotations"], response_model=list[dict], status_code=200)
async def get_annotations():
    """
    Get all annotations from annotation corpus
    """
    logger.info(f"Getting all annotations from annotation corpus")

    annotations_collection = get_collection(ANNOTATIONS_COLLECTION_NAME)
    annotations_data = list(annotations_collection.find())

    if annotations_data is None:
        logger.error(f"No annotations found in annotation index")
        raise HTTPException(status_code=404, detail=f"No annotations found in annotation index")

    return annotations_data


@router.get("/annotations/{annotation_id}", tags=["annotations"], response_model=dict, status_code=200)
async def get_annotation(annotation_id: str):
    """
    Get annotation from annotation corpus
    """
    logger.info(f"Getting annotation data for annotation_id: {annotation_id}")

    annotations_collection = get_collection(ANNOTATIONS_COLLECTION_NAME)
    annotation_data = annotations_collection.find_one({"annotation_id": annotation_id})

    if annotation_data is None:
        logger.error(f"Annotation {annotation_id} not found in annotation index")
        raise HTTPException(status_code=404, detail=f"Annotation {annotation_id} not found in annotation index")

    return annotation_data


@router.post("/annotations", tags=["annotations"], response_model=dict, status_code=201)
async def post_annotation(fields=Body(...)):
    """
    Post annotation to annotation corpus.

    Body should contain the data fields to be posted to the annotation.
    """
    logger.info(f"Posting annotation data")

    if fields is None or len(fields) == 0:
        logger.error(f"Fields not specified")
        raise HTTPException(status_code=422, detail=f"Fields not specified")

    if "annotation_id" not in fields:
        logger.error(f"Annotation ID not specified")
        raise HTTPException(status_code=422, detail=f"Field 'annotation_id' must be specified")

    annotations_collection = get_collection(ANNOTATIONS_COLLECTION_NAME)
    annotations_collection.insert_one(fields)

    # get newly created annotation
    new_annotation = annotations_collection.find_one({"annotation_id": fields["annotation_id"]})

    return new_annotation


@router.put("/annotations/{annotation_id}", tags=["annotations"], response_model=dict, status_code=200)
async def put_annotation(annotation_id: str, fields=Body(...)):
    """
    Put annotation to annotation corpus.

    Body should contain the data fields to be posted to the annotation.
    """
    logger.info(f"Putting annotation data for annotation_id: {annotation_id}")

    if fields is None or len(fields) == 0:
        logger.error(f"Fields not specified")
        raise HTTPException(status_code=422, detail=f"Fields not specified")

    annotations_collection = get_collection(ANNOTATIONS_COLLECTION_NAME)
    old_annotation = annotations_collection.find_one({"annotation_id": annotation_id})
    if old_annotation is None:
        logger.error(f"Annotation {annotation_id} not found in annotation index, adding new annotation...")
        return await post_annotation(fields=fields | {"annotation_id": annotation_id})

    new_annotation = old_annotation | fields
    annotations_collection.update_one({"annotation_id": annotation_id}, {"$set": fields})

    # get updated annotation
    updated_annotation = annotations_collection.find_one({"annotation_id": annotation_id})

    return updated_annotation


@router.delete("/annotations/{annotation_id}", tags=["annotations"], response_model=dict, status_code=200)
async def delete_annotation(annotation_id: str):
    """
    Delete annotation from annotation corpus
    """
    logger.info(f"Deleting annotation data for annotation_id: {annotation_id}")

    annotations_collection = get_collection(ANNOTATIONS_COLLECTION_NAME)
    annotation_data = annotations_collection.find_one({"annotation_id": annotation_id})

    if annotation_data is None:
        logger.error(f"Annotation {annotation_id} not found in annotation index")
        raise HTTPException(status_code=404, detail=f"Annotation {annotation_id} not found in annotation index")

    annotations_collection.delete_one({"annotation_id": annotation_id})

    return annotation_data
