from fastapi import APIRouter, HTTPException, Body

from core.utils.constants import DOCUMENT_CORPUS_COLLECTION_NAME
from core.utils.logging import get_logger
from api.storage_api.utils.database_connection import get_collection
from api.storage_api.utils.parsing import parse

logger = get_logger(__name__)

router = APIRouter()


@router.get("/documents", tags=["documents"], response_model=list[dict], status_code=200)
async def get_documents():
    """
    Get all documents from document corpus
    """

    logger.info(f"Getting all documents from document corpus")

    document_corpus_collection = get_collection(DOCUMENT_CORPUS_COLLECTION_NAME)
    documents_data = list(document_corpus_collection.find())

    if documents_data is None:
        logger.error(f"No documents found in document index")
        raise HTTPException(status_code=404, detail=f"No documents found in document index")

    return parse(documents_data)


@router.get("/documents/{document_id}", tags=["documents"], response_model=dict, status_code=200)
async def get_document(document_id: str):
    """
    Get document from document corpus
    """

    logger.info(f"Getting document data for document_id: {document_id}")

    document_corpus_collection = get_collection(DOCUMENT_CORPUS_COLLECTION_NAME)
    document_data = document_corpus_collection.find_one({"document_id": document_id})

    if document_data is None:
        logger.error(f"Document {document_id} not found in document index")
        raise HTTPException(status_code=404, detail=f"Document {document_id} not found in document index")

    return parse(document_data)


@router.post("/documents", tags=["documents"], response_model=dict, status_code=201)
async def post_document(fields=Body(...)):
    """
    Post document to document corpus.

    Body should contain the data fields to be posted to the document.
    """

    if fields is None:
        logger.error(f"Fields not specified")
        raise HTTPException(status_code=422, detail=f"Fields not specified")

    if len(fields) == 0:
        logger.error(f"Fields cannot be empty")
        raise HTTPException(status_code=422, detail=f"Fields cannot be empty")

    if "doc_id" not in fields:
        logger.error(f"Document ID not specified")
        raise HTTPException(status_code=422, detail=f"Field 'doc_id' must be specified")

    logger.info(f"Posting document data for document_id: {fields['doc_id']}")

    doc_id = fields["doc_id"]
    document_corpus_collection = get_collection(DOCUMENT_CORPUS_COLLECTION_NAME)
    document_data = document_corpus_collection.find_one({"doc_id": doc_id})

    if document_data is not None:
        logger.error(f"Document {doc_id} already exists in document index")
        raise HTTPException(status_code=409,
                            detail=f"Document {doc_id} already exists in document index,"
                                   f" use PUT instead to update it.")

    document_data = {
        "document_id": doc_id,
        **fields
    }

    document_corpus_collection.insert_one(document_data)

    return parse(document_data)


@router.put("/documents/{doc_id}", tags=["documents"], response_model=dict, status_code=200)
async def put_document(doc_id: str, fields=Body(...)):
    """
    Put document to document corpus.

    Body should contain the data fields to be put to the document.
    """

    logger.info(f"Updating document data for document_id: {doc_id}")

    if fields is None:
        logger.error(f"Fields not specified")
        raise HTTPException(status_code=422, detail=f"Fields not specified")

    if len(fields) == 0:
        logger.error(f"Fields cannot be empty")
        raise HTTPException(status_code=422, detail=f"Fields cannot be empty")

    document_corpus_collection = get_collection(DOCUMENT_CORPUS_COLLECTION_NAME)
    document_data = document_corpus_collection.find_one({"doc_id": doc_id})

    if document_data is None:
        logger.error(f"Document {doc_id} does not exist in document index, creating it...")
        return await post_document(doc_id, fields)

    new_document_data = document_data | fields

    document_corpus_collection.update_one({"doc_id": doc_id}, {"$set": new_document_data})

    return parse(document_data)


@router.delete("/documents/{doc_id}", tags=["documents"], status_code=204)
async def delete_document(doc_id: str):
    """
    Delete document from document corpus
    """

    logger.info(f"Deleting document data for doc_id: {doc_id}")

    document_corpus_collection = get_collection(DOCUMENT_CORPUS_COLLECTION_NAME)
    document_data = document_corpus_collection.find_one({"doc_id": doc_id})

    if document_data is None:
        logger.error(f"Document {doc_id} does not exist in document index")
        raise HTTPException(status_code=404, detail=f"Document {doc_id} does not exist in document index")

    document_corpus_collection.delete_one({"doc_id": doc_id})

    return {}
