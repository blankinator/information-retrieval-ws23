from fastapi import FastAPI
import uvicorn

from core.utils.constants import MAIN_API_PORT
from core.utils.logging import get_logger

logger = get_logger(__name__)
app = FastAPI()


@app.get("/")
async def root():
    return {"message": "Hello World"}


if __name__ == "__main__":
    logger.info(f"Starting main api at port {MAIN_API_PORT}")
    uvicorn.run(app, host="0.0.0.0", port=MAIN_API_PORT)
