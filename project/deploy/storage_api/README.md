# Storage API Deployment

This document describes how to deploy the Storage API.

## Prerequisites

- [Docker](https://docs.docker.com/install/)
- [Docker Compose](https://docs.docker.com/compose/install/)

## Deployment

1. Clone the repository
2. Create .env file in the storage_api directory
    - See [Environment Variables](#environment-variables) for a list of required variables
    - Set `STORAGE_API_DB_HOST` to the 'mongodb' service name if using the provided docker-compose file
    - Set `STORAGE_API_DB_HOST` and `STORAGE_API_DB_PORT` to the host and port of the database if not using the provided
      docker-compose file
3. Run `docker-compose up -d` in the storage_api directory
4. Documentation is available at `http://{HOST}:{PORT}/docs`

## Environment Variables

| Variable              | Description                                                |
|-----------------------|------------------------------------------------------------|
| `STORAGE_API_PORT`    | Port to run the Storage API on                             |
| `STORAGE_API_DB_HOST` | Hostname of the database                                   |
| `STORAGE_API_DB_PORT` | Port of the database                                       |
| `STORAGE_API_DB_NAME` | Name of the database (defaults to `information retrieval`) |