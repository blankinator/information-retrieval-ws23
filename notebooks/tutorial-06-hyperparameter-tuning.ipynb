{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tutorial on Tuning Hyperparameters of Retrieval Systems\n",
    "\n",
    "This tutorial exemplifies hyperparameter tuning with PyTerrier and TrecTools.\n",
    "To make things a bit more explicit, we exhaustively evaluate a small grid of possible parameters for BM25.\n",
    "After you understand the concepts of this tutorial, please consider to switch to a dedicated API for tuning hyperparameters, e.g., [the official PyTerrier one](https://pyterrier.readthedocs.io/en/latest/tuning.html).\n",
    "\n",
    "**Attention:** This tutorial comes in two parts, where part 1 executes all configurations and part 2 does the actual search. Please skim only over part 1 (and do not execute it) if you do this tutorial for the first time and come back later if needed, as we prepared the results of part 2 via a download so that you directly can skip to part 2."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Preparation: Install dependencies"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# This is only needed in Google Colab, in the dev container, everything should be installed already\n",
    "!pip3 install tira trectools python-terrier"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Our Scenario\n",
    "\n",
    "We want to tune the hyperparameters of BM25 on the training and validation data of the [IR Lab of the winter semester Jena/Leipzig](https://www.tira.io/task-overview/ir-lab-jena-leipzig-wise-2023).\n",
    "\n",
    "First, we import all used dependencies:\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from tira.third_party_integrations import ir_datasets, ensure_pyterrier_is_loaded, persist_and_normalize_run\n",
    "import pyterrier as pt\n",
    "\n",
    "ensure_pyterrier_is_loaded()\n",
    "\n",
    "training_dataset = 'ir-lab-jena-leipzig-wise-2023/training-20231104-training'\n",
    "validation_dataset = 'ir-lab-jena-leipzig-wise-2023/validation-20231104-training'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Part 1: Run all Configurations of the Grid Search\n",
    "\n",
    "Running all configurations below takes roughly three hours (that is one advantage of dedicated APIs, they often offer parallelization). Therefore, please only skim over this first part if you do the tutorial for the first time, you can download the outputs of this grid search at the beginning of part 2 so that you directly can skip to part 2.\n",
    "\n",
    "Next, we implement two methods: (1) for indexing documents, and (2) for grid search method to exhaustively run a small grid of possible parameters for BM25. We will store all runs in a directory `grid-search/training` (for the training runs) respectively `grid-search/validation` (for the validation runs)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def create_index(documents):\n",
    "    indexer = pt.IterDictIndexer(\"/tmp/index\", overwrite=True, meta={'docno': 100, 'text': 20480})\n",
    "    index_ref = indexer.index(({'docno': i.doc_id, 'text': i.text} for i in documents))\n",
    "    return pt.IndexFactory.of(index_ref)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def run_bm25_grid_search_run(index, output_dir, queries):\n",
    "    \"\"\"\n",
    "        defaults: http://terrier.org/docs/current/javadoc/org/terrier/matching/models/BM25.html\n",
    "        k_1 = 1.2d, k_3 = 8d, b = 0.75d\n",
    "        We do not tune parameter k_3, as this parameter only impacts queries with reduntant terms.\n",
    "    \"\"\"\n",
    "    for b in [0.7, 0.75, 0.8]:\n",
    "        for k_1 in [1.1, 1.2, 1.3]:\n",
    "            system = f'bm25-b={b}-k_1={k_1}'\n",
    "            configuration = {\"bm25.b\" : b, \"bm25.k_1\": k_1}\n",
    "            run_output_dir = output_dir + '/' + system\n",
    "            !rm -Rf {run_output_dir}\n",
    "            !mkdir -p {run_output_dir}\n",
    "            print(f'Run {system}')\n",
    "            BM25 = pt.BatchRetrieve(index, wmodel=\"BM25\", controls=configuration, verbose=True)\n",
    "            run = BM25(queries)\n",
    "            persist_and_normalize_run(run, system, run_output_dir)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Run All Configurations on the Training Data\n",
    "\n",
    "First, we load the training dataset and index the documents, then we run our `run_bm25_grid_search_run`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dataset = ir_datasets.load(training_dataset)\n",
    "queries = pt.io.read_topics(ir_datasets.topics_file(training_dataset), format='trecxml')\n",
    "\n",
    "queries.head(3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "index = create_index(dataset.docs_iter())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "run_bm25_grid_search_run(index, 'grid-search/training', queries)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Run All Configurations on the Validation Data\n",
    "\n",
    "Second, we load the validation dataset and index the documents, then we run our `run_bm25_grid_search_run`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dataset = ir_datasets.load(validation_dataset)\n",
    "queries = pt.io.read_topics(ir_datasets.topics_file(validation_dataset), format='trecxml')\n",
    "\n",
    "queries.head(3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "index = create_index(dataset.docs_iter())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "run_bm25_grid_search_run(index, 'grid-search/validation', queries)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Part 2: Evaluate all Configurations of the Grid Search\n",
    "\n",
    "First, we import the dependencies and load the training and validation qrels."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from trectools import TrecRun, TrecQrel, TrecEval\n",
    "from tira.rest_api_client import Client\n",
    "from glob import glob\n",
    "import pandas as pd\n",
    "tira = Client()\n",
    "\n",
    "def load_qrels(dataset):\n",
    "    return TrecQrel(tira.download_dataset('ir-lab-jena-leipzig-wise-2023', dataset, truth_dataset=True) + '/qrels.txt')\n",
    "\n",
    "training_qrels = load_qrels('training-20231104-training')\n",
    "validation_qrels = load_qrels('validation-20231104-training')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We download the rusn of the grid search and evaluate them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!wget https://files.webis.de/teaching/ir-wise-23/ir-lab-sose-grid-search.zip\n",
    "!unzip ir-lab-sose-grid-search.zip"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def evaluate_run(run_dir, qrels):\n",
    "    run = TrecRun(run_dir + '/run.txt')\n",
    "    trec_eval = TrecEval(run, qrels)\n",
    "\n",
    "    return {\n",
    "        'run': run.get_runid(),\n",
    "        'nDCG@10': trec_eval.get_ndcg(depth=10),\n",
    "        'nDCG@10 (unjudgedRemoved)': trec_eval.get_ndcg(depth=10, removeUnjudged=True),\n",
    "        'MAP': trec_eval.get_map(depth=10),\n",
    "        'MRR': trec_eval.get_reciprocal_rank()\n",
    "    }"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = []\n",
    "for r in glob('grid-search/training/bm25*'):\n",
    "    df += [evaluate_run(r, training_qrels)]\n",
    "df = pd.DataFrame(df)\n",
    "df.sort_values('nDCG@10', ascending=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = []\n",
    "for r in glob('grid-search/validation/bm25*'):\n",
    "    df += [evaluate_run(r, validation_qrels)]\n",
    "df = pd.DataFrame(df)\n",
    "df.sort_values('nDCG@10', ascending=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Summary\n",
    "\n",
    "We conducted an exhaustive grid search on the b and k1 parameters of BM25.\n",
    "\n",
    "To summarize everything, please answer the following three questions:\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Question 1:\n",
    "\n",
    "What are the advantages of splitting  into a training and validation set?\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Question 2:\n",
    "\n",
    "Are there scenarious where you would join the training and validation data?"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
